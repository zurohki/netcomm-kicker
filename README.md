# Netcomm-Kicker

Reboot a Netcomm modem/router.

I designed this for an NF10WV, it might work on other Netcomm devices.

Best run on a timer, like cron.

Rename config.ini.EDIT_AND_RENAME to config.ini and put the appropriate
settings for your modem in it.


```
Usage: ./checkmodem.py [command]

Command:
  -h --help              This information.
  --reboot               Reboot regardless of current state.
  --reconnect            Reconnect DSL regardless of current state.
  --check-only           Only check IPv6, don't change anything.
  --check-and-reboot     (Default) Check IPv6 and reboot if needed.
  --check-and-reconnect  Check IPv6 and reconnect DSL if needed.
```
