#!/usr/bin/python3

import telnetlib
import time
import sys
import configparser
from os.path import isfile, isdir, dirname, join
from distutils.util import strtobool
import sys

debugLogging = True

def connect(A, user, password):
    try:
        tn = telnetlib.Telnet(A)
    except OSError as err:
        print(err)
        print('Unable to connect to: ' + A)
        exit(1)
    logger(tn.read_until(b'Login:', 5))
    tn.write(user + b'\n')
    logger(tn.read_until(b'Password:', 5))
    tn.write(password + b'\n')
    loginResult = tn.read_until(b'> ', 5)
    logger(loginResult)
    if b'Login incorrect. Try again.' in loginResult:
        print('Login failed. Exiting.')
        disconnect(tn)
        exit(1)
    return tn

def disconnect(tn):
    tn.write(b'exit' + b'\n')
    time.sleep(3)
    try:
        logger(tn.read_very_eager())
        tn.close()
    except Exception as err:
        # Errors disconnecting probably don't matter
        logger(err)
        pass
    return

def checkIPv6(tn):
    print('Logged in, checking IPv6.')
    tn.write(b'sh' + b'\n')
    logger(tn.read_until(b'# ', 5))
    tn.write(b'ip -6 route | grep "default via"'  + b'\n')
    time.sleep(5)
    logger(tn.read_until(b'\n'))
    ipv6Text = tn.read_until(b'# ')
    logger(ipv6Text)
    ipv6TextCount = ipv6Text.count(b'default via')
    logger(ipv6TextCount)
    tn.write(b'exit' + b'\n')
    logger(tn.read_until(b'> ', 5))
    if ipv6TextCount == 1:
        logger('Default ipv6 route found.')
        print('IPv6 check done, looks fine.')
        return True
    else:
        logger('Default ipv6 route not found.')
        print('IPv6 not working.')
        return False

def restartDSL(tn):
    # VDSL didn't come back up on my NF10VW
    print('Restarting DSL.')
    tn.write(b'adsl stop' + b'\n')
    logger(tn.read_until(b'> ', 5))
    time.sleep(25)
    tn.write(b'adsl start' + b'\n')
    logger(tn.read_until(b'> ', 5))
    return

def reboot(tn):
    print('Rebooting router.')
    tn.write(b'sh' + b'\n')
    logger(tn.read_until(b'# ', 5))
    tn.write(b'reboot' + b'\n')
    time.sleep(1)
    logger(tn.read_until(b'\n'))
    return

def logger(msg):
    global debugLogging
    if debugLogging:
        x = msg
        if type(x) is bytes:
            x = x.decode('utf-8')
        x = str(x)
        msglines = x.splitlines()
        for line in msglines:
            print('Debug: ' + line)
    return msg

def findConfig():
    configFile = 'config.ini'

    if isfile(configFile):
        return configFile

    if isfile(join(dirname(__file__), configFile)):
        return join(dirname(__file__), configFile)

    print('Failed to read configuration file. Are you sure there is a file called "config.ini"?\n')
    exit(1)
    return

def readConfig():
    configFile = findConfig()
    config = configparser.ConfigParser()
    config.read(configFile)
    myconfig = config['netcomm-kicker']
    try:
        debug = strtobool(myconfig['debugLogging'])
        modemIP = myconfig['modemIP']
        user = myconfig['user'].encode('utf-8')
        password = myconfig['password'].encode('utf-8')
    except Exception:
        print('Failed to parse configuration file. Is it broken?\n')
        logger(myconfig)
        exit(1)
    return modemIP, user, password, debug

def showHelp():
    helpText = "Usage: " + sys.argv[0] + " [command]\n\n"
    helpText += "Command:\n"
    helpText += "  " + "-h --help              This information.\n"
    helpText += "  " + "--reboot               Reboot regardless of current state.\n"
    helpText += "  " + "--reconnect            Reconnect DSL regardless of current state.\n"
    helpText += "  " + "--check-only           Only check IPv6, don't change anything.\n"
    helpText += "  " + "--check-and-reboot     (Default) Check IPv6 and reboot if needed.\n"
    helpText += "  " + "--check-and-reconnect  Check IPv6 and reconnect DSL if needed.\n"
    print(helpText)
    return

if '--help' in sys.argv or '-h' in sys.argv or '-help' in sys.argv:
    showHelp()
    exit(0)

if len(sys.argv) > 2:
    print('Too many arguments.')
    exit(1)

validArgs = ['-h', '--help', '--reboot', '--reconnect', '--check-only', '--check-and-reboot', '--check-and-reconnect']

if len(sys.argv) > 1 and sys.argv[1] not in validArgs:
    print('Unrecognised argument: ' + sys.argv[1])
    showHelp()
    exit(1)

modemIP, user, password, debugLogging = readConfig()
logger(modemIP)
logger(user)
logger(password)
logger(debugLogging)
logger(sys.argv)
tnModem = connect(modemIP, user, password)

if len(sys.argv) == 1:
    ipv6Working = checkIPv6(tnModem)
    logger(ipv6Working)
    if not ipv6Working:
        reboot(tnModem)
    print('Disconnecting.')
    disconnect(tnModem)
    print('All done.')
    exit(0)

if sys.argv[1] == "--check-only":
    ipv6Working = checkIPv6(tnModem)
    logger(ipv6Working)
elif sys.argv[1] == "--check-and-reboot":
    ipv6Working = checkIPv6(tnModem)
    logger(ipv6Working)
    if not ipv6Working:
        reboot(tnModem)
elif sys.argv[1] == "--check-and-reconnect":
    ipv6Working = checkIPv6(tnModem)
    logger(ipv6Working)
    if not ipv6Working:
        restartDSL(tnModem)
elif sys.argv[1] == "--reboot":
    reboot(tnModem)
elif sys.argv[1] == "--reconnect":
    restartDSL(tnModem)

print('Disconnecting.')
disconnect(tnModem)
print('All done.')
exit(0)
